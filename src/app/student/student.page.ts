import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.page.html',
  styleUrls: ['./student.page.scss'],
})
export class StudentPage implements OnInit {

  constructor(private apiService : ApiServiceService, private router: Router) { }
  todo = {
    title: '',
    description: ''
  };
  ngOnInit() {
  }
  
  logForm() {
    this.apiService.setStudent(this.todo);
    console.log(this.todo);
    this.router.navigate(['/student-details']);
  }

}
